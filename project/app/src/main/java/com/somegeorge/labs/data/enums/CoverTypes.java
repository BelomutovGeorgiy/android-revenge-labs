package com.somegeorge.labs.data.enums;

/**
 * Created by georg on 22.10.2017.
 */

public enum CoverTypes {
    Soft,
    Hard
}
