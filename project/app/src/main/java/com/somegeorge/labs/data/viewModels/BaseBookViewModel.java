package com.somegeorge.labs.data.viewModels;

import android.app.Activity;
import android.content.Context;
import android.databinding.BaseObservable;
import android.databinding.BindingAdapter;
import android.support.v7.widget.RecyclerView;

import com.somegeorge.labs.Startup;
import com.somegeorge.labs.adapters.BookRecyclerViewAdapter;
import com.somegeorge.labs.data.models.Book;
import com.somegeorge.labs.data.repositories.IAuthorRepository;
import com.somegeorge.labs.data.repositories.IBookRepository;
import com.somegeorge.labs.data.repositories.IPublisherRepository;
import com.somegeorge.labs.utils.di.DaggerIDaggerComponent;

import java.util.List;
import java.util.stream.Collectors;

import javax.inject.Inject;

/**
 * Created by georg on 22.10.2017.
 */

public class BaseBookViewModel extends BaseObservable {

    @Inject public IBookRepository bookRepository;
    @Inject public IAuthorRepository authorRepository;
    @Inject public IPublisherRepository publisherRepository;

    protected static Activity context;

    public BaseBookViewModel(Activity parent) {
        this.context = parent;
        ((Startup)parent.getApplication()).getComponent().inject(this);

        bookRepository.getAll().forEach(b -> b.setAuthor(authorRepository.get(b.getAuthorId())));
        bookRepository.getAll().forEach(b -> b.setPublisher(publisherRepository.get(b.getPublisherId())));
        authorRepository.getAll().forEach(a -> a.setBooks(bookRepository.getAllByAuthorId(a.getId())));
    }

    @BindingAdapter("bind:books")
    public static void books(RecyclerView view, List<Book> books){
        if(books!=null) {
            BookRecyclerViewAdapter adapter = new BookRecyclerViewAdapter(context, books);
            view.setAdapter(adapter);
        }
    }
}
