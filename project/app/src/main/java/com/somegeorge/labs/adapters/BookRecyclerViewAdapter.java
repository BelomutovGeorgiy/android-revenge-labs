package com.somegeorge.labs.adapters;

import android.app.Activity;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.somegeorge.labs.data.models.Book;
import com.somegeorge.labs.databinding.BookCardLayoutBinding;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by georg on 20.10.2017.
 */

public class BookRecyclerViewAdapter extends RecyclerView.Adapter<BookRecyclerViewViewHolder> {

    private final List<Book> books;
    private Activity context;

    public BookRecyclerViewAdapter(Activity context, List<Book> books) {
        this.books = new ArrayList<>(books);
        this.context = context;
    }

    @Override
    public BookRecyclerViewViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        BookCardLayoutBinding binding = BookCardLayoutBinding.inflate(inflater, parent, false);
        return new BookRecyclerViewViewHolder(binding, context);
    }

    @Override
    public void onBindViewHolder(BookRecyclerViewViewHolder holder, int position) {
        Book book = books.get(position);
        holder.bind(book);
    }

    @Override
    public int getItemCount() {
        return books.size();
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }
}
