package com.somegeorge.labs.data.repositories;

import java.util.List;

/**
 * Created by georg on 22.10.2017.
 */

public interface IRepository<T> {
    List<T> getAll();
    T get(int id);
    void update(int id, T object);
    void add(T object);
    void delete(int id);
}
