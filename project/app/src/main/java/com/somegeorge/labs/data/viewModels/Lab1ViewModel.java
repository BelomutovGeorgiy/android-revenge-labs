package com.somegeorge.labs.data.viewModels;

import android.databinding.BaseObservable;

import com.somegeorge.labs.data.models.User;

/**
 * Created by georg on 14.10.2017.
 */

public class Lab1ViewModel extends BaseObservable {

    public User getUser() {
        return user;
    }

    private User user;

    public void createHeorhi(){
        user = new User("Heorhi", "Belamutau");
        notifyChange();
    }
}
