package com.somegeorge.labs.adapters;

import android.view.View;

/**
 * Created by georg on 24.10.2017.
 */

public interface RecyclerClickListener {
    void onClick(View view, int position);
    void onLongClick(View view, int position);
}
