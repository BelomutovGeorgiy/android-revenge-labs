package com.somegeorge.labs;

import android.app.Application;

import com.somegeorge.labs.utils.di.DaggerIDaggerComponent;
import com.somegeorge.labs.utils.di.IDaggerComponent;
import com.somegeorge.labs.utils.di.modules.RepositoryModule;

/**
 * Created by georg on 22.10.2017.
 */

public class Startup extends Application {

    public IDaggerComponent getComponent() {
        return component;
    }

    private IDaggerComponent component;

    @Override
    public void onCreate(){
        super.onCreate();

        buildDependencies();
    }

    private void buildDependencies(){
        component = DaggerIDaggerComponent.builder()
                .build();
        component.inject(this);
    }
}
