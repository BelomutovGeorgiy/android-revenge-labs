package com.somegeorge.labs.data.viewModels;

import android.app.Activity;
import android.databinding.BaseObservable;
import android.databinding.Bindable;
import android.databinding.BindingAdapter;
import android.databinding.InverseBindingAdapter;
import android.databinding.InverseBindingListener;
import android.databinding.InverseBindingMethod;
import android.databinding.InverseBindingMethods;
import android.support.v7.widget.AppCompatSpinner;
import android.view.View;
import android.widget.AdapterView;

import com.somegeorge.labs.BR;
import com.somegeorge.labs.Startup;
import com.somegeorge.labs.data.models.Author;
import com.somegeorge.labs.data.models.Book;
import com.somegeorge.labs.data.models.Publisher;
import com.somegeorge.labs.data.repositories.AuthorRepository;
import com.somegeorge.labs.data.repositories.BookRepository;
import com.somegeorge.labs.data.repositories.PublisherRepository;
import com.somegeorge.labs.utils.di.DaggerIDaggerComponent;
import com.somegeorge.labs.utils.di.IDaggerComponent;
import com.somegeorge.labs.utils.di.modules.RepositoryModule;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import javax.inject.Inject;

/**
 * Created by georg on 15.10.2017.
 */
@InverseBindingMethods({
        @InverseBindingMethod(type = AppCompatSpinner.class, attribute = "android:selectedItemPosition"),
})
public abstract class Lab2ViewModel extends BaseBookViewModel implements IViewModel {

    protected List<Book> books;
    protected List<Author> authors;
    protected List<Publisher> publishers;
    protected Author selectedAuthor;
    protected int selectedAuthorPosition;

    public List<Book> getBooks() {
        return books;
    }

    public List<Author> getAuthors() {return authors;}

    @Bindable
    public int getSelectedAuthorPosition() {
        return selectedAuthorPosition;
    }

    public void setSelectedAuthorPosition(int selectedAuthorPosition) {
        this.selectedAuthorPosition = selectedAuthorPosition;
    }

    @Bindable
    public Author getSelectedAuthor() {
        return selectedAuthor;
    }

    public void setSelectedAuthor(Author value){
        this.selectedAuthor = value;
        notifyPropertyChanged(BR.selectedAuthor);
    }

    public Lab2ViewModel(Activity parent) {
        super(parent);
        books = bookRepository.getAll();
        authors = authorRepository.getAll();
        publishers = publisherRepository.getAll();

        books.forEach(b -> b.setAuthor(authors.get(b.getAuthorId())));
        books.forEach(b -> b.setPublisher(publishers.get(b.getPublisherId())));
        authors.forEach(a -> a.setBooks(books.stream().filter(b -> b.getAuthorId() == a.getId()).collect(Collectors.toList())));
        selectedAuthorPosition = 0;
    }

    @BindingAdapter("selectedItemPositionAttrChanged")
    public void setSelectedItemPositionListener(AppCompatSpinner view, final InverseBindingListener selectedItemPositionChange) {
        if (selectedItemPositionChange == null) {
            view.setOnItemSelectedListener(null);
        } else {
            view.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    selectedItemPositionChange.onChange();
                    setSelectedAuthorPosition(position);
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });
        }
    }

    @InverseBindingAdapter(attribute = "selectedItemPosition")
    public static int getSelectedItemPosition(AppCompatSpinner spinner) {
        return spinner.getSelectedItemPosition();
    }


}
