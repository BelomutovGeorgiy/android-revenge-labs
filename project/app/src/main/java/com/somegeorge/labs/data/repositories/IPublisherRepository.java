package com.somegeorge.labs.data.repositories;

import com.somegeorge.labs.data.models.Publisher;

/**
 * Created by georg on 22.10.2017.
 */

public interface IPublisherRepository extends  IRepository<Publisher> {

}
