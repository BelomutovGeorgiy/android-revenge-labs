package com.somegeorge.labs;

import android.app.Fragment;
import android.databinding.DataBindingUtil;
import android.support.annotation.Nullable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.somegeorge.labs.data.viewModels.Lab1ViewModel;
import com.somegeorge.labs.databinding.ActivityLab1Binding;

public class Lab1Activity extends Fragment {

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        return inflater.inflate(R.layout.activity_lab1, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState){
        super.onViewCreated(view, savedInstanceState);

        ActivityLab1Binding binding = DataBindingUtil.bind(view);
        binding.setBinding(new Lab1ViewModel());
    }
}
