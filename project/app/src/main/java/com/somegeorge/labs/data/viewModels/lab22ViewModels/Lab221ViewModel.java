package com.somegeorge.labs.data.viewModels.lab22ViewModels;

import android.app.Activity;
import android.content.Context;
import android.databinding.BaseObservable;
import android.databinding.BindingAdapter;
import android.support.v7.widget.RecyclerView;
import android.util.Log;

import com.somegeorge.labs.adapters.BookRecyclerViewAdapter;
import com.somegeorge.labs.data.models.Author;
import com.somegeorge.labs.data.models.Book;
import com.somegeorge.labs.data.viewModels.BaseBookViewModel;

import java.util.List;

/**
 * Created by georg on 21.10.2017.
 */

public class Lab221ViewModel extends BaseBookViewModel {

    public List<Book> getBooks() {
        return books;
    }

    public void setBooks(List<Book> books) {
        this.books = books;
    }

    public Lab221ViewModel(List<Book> books, Activity parent) {
        super(parent);
        this.books = books;
    }

    private List<Book> books;

    @BindingAdapter("bind:books")
    public static void books(RecyclerView view, List<Book> books){
        if(books!=null) {
            BookRecyclerViewAdapter adapter = new BookRecyclerViewAdapter(context, books);
            view.setAdapter(adapter);
        }
    }
}
