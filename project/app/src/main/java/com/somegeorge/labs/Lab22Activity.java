package com.somegeorge.labs;

import android.app.Fragment;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.somegeorge.labs.data.viewModels.Lab22ViewModel;
import com.somegeorge.labs.data.viewModels.Lab2ViewModel;
import com.somegeorge.labs.databinding.ActivityLab22Binding;
import com.somegeorge.labs.databinding.ActivityLab2Binding;

/**
 * Created by georg on 21.10.2017.
 */

public class Lab22Activity extends Fragment {
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        return inflater.inflate(R.layout.activity_lab2_2, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState){
        super.onViewCreated(view, savedInstanceState);
        ActivityLab22Binding binding = DataBindingUtil.bind(view);
        binding.setBinding(new Lab22ViewModel(getActivity()));
    }
}
