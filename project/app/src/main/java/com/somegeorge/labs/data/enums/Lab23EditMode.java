package com.somegeorge.labs.data.enums;

/**
 * Created by georg on 22.10.2017.
 */

public enum Lab23EditMode {
    CREATE,
    EDIT
}
