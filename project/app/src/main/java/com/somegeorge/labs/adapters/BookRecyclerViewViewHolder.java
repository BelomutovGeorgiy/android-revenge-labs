package com.somegeorge.labs.adapters;

import android.app.Activity;
import android.content.Context;
import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;

import com.somegeorge.labs.data.models.Book;
import com.somegeorge.labs.data.viewModels.BookViewModel;
import com.somegeorge.labs.databinding.BookCardLayoutBinding;

/**
 * Created by georg on 20.10.2017.
 */

public class BookRecyclerViewViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
    private final BookCardLayoutBinding binding;
    private final Activity context;

    public BookRecyclerViewViewHolder(BookCardLayoutBinding binding, Activity context) {
        super(binding.getRoot());
        this.binding = binding;
        this.context = context;
    }

    public void bind(Book book){
        binding.setBinding(new BookViewModel(context, book));
        binding.executePendingBindings();
    }

        @Override
        public void onClick(View v) {

        }

}
