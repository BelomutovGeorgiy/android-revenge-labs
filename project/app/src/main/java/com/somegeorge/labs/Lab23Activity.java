package com.somegeorge.labs;

import android.app.Fragment;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.somegeorge.labs.data.viewModels.lab23ViewModels.Lab23ViewModel;
import com.somegeorge.labs.databinding.ActivityLab23ReadBinding;

/**
 * Created by georg on 22.10.2017.
 */

public class Lab23Activity extends Fragment {

    private View view;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        return inflater.inflate(R.layout.activity_lab2_3_read, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState){
        super.onViewCreated(view, savedInstanceState);
        this.view = view;
        ActivityLab23ReadBinding binding = DataBindingUtil.bind(view);
        binding.setBinding(new Lab23ViewModel(this.getActivity()));
    }

    @Override
    public void onResume() {
        super.onResume();
        ActivityLab23ReadBinding binding = DataBindingUtil.bind(view);
        binding.setBinding(new Lab23ViewModel(this.getActivity()));
    }
}
