package com.somegeorge.labs.data.models;

import java.io.Serializable;

/**
 * Created by georg on 22.10.2017.
 */

public class Publisher implements Serializable {
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Publisher(int id, String name) {

        this.id = id;
        this.name = name;
    }
    public Publisher(){}

    private int id;
    private String name;

    @Override
    public String toString() {
        return this.name;
    }
}
