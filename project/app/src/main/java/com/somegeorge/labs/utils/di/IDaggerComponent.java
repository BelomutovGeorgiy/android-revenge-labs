package com.somegeorge.labs.utils.di;

import com.somegeorge.labs.Startup;
import com.somegeorge.labs.data.repositories.IAuthorRepository;
import com.somegeorge.labs.data.repositories.IBookRepository;
import com.somegeorge.labs.data.repositories.IPublisherRepository;
import com.somegeorge.labs.data.viewModels.BaseBookViewModel;
import com.somegeorge.labs.data.viewModels.Lab2ViewModel;
import com.somegeorge.labs.data.viewModels.lab23ViewModels.Lab23EditViewModel;
import com.somegeorge.labs.data.viewModels.lab23ViewModels.Lab23ViewModel;
import com.somegeorge.labs.utils.di.modules.RepositoryModule;

import javax.inject.Singleton;

import dagger.Component;

@Singleton
@Component(modules = { RepositoryModule.class })
public interface IDaggerComponent {
    void inject(Lab2ViewModel viewModel);
    void inject(Startup startup);
    void inject(Lab23ViewModel viewModel);
    void inject(Lab23EditViewModel viewModel);
    void inject(BaseBookViewModel viewModel);
}
