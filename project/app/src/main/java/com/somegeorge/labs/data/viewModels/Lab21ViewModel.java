package com.somegeorge.labs.data.viewModels;

import android.app.Activity;
import android.databinding.BindingAdapter;
import android.support.v4.content.Loader;
import android.support.v7.widget.RecyclerView;
import android.util.Log;

import com.somegeorge.labs.adapters.BookRecyclerViewAdapter;
import com.somegeorge.labs.data.models.Author;
import com.somegeorge.labs.data.models.Book;

import java.util.List;

/**
 * Created by georg on 21.10.2017.
 */

public class Lab21ViewModel extends Lab2ViewModel {

    public Lab21ViewModel(Activity parent) {
        super(parent);
    }

    public void updateAuthor(){
        setSelectedAuthor(authors.get(selectedAuthorPosition));
    }

    @BindingAdapter("bind:books")
    public static void books(RecyclerView view, List<Book> books){
        if(books!=null) {
            BookRecyclerViewAdapter adapter = new BookRecyclerViewAdapter(context, books);
            Log.d("BINDNIG ADAPTER", "I'm coming!!");
            view.setAdapter(adapter);
        }
    }
}
