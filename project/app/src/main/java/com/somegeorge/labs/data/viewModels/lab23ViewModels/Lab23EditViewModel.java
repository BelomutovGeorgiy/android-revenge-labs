package com.somegeorge.labs.data.viewModels.lab23ViewModels;

import android.app.Activity;
import android.content.Context;
import android.databinding.BaseObservable;
import android.databinding.Bindable;
import android.databinding.BindingAdapter;
import android.databinding.InverseBindingAdapter;
import android.databinding.InverseBindingListener;
import android.support.v7.widget.AppCompatSpinner;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Toast;

import com.somegeorge.labs.R;
import com.somegeorge.labs.Startup;
import com.somegeorge.labs.adapters.BookRecyclerViewAdapter;
import com.somegeorge.labs.data.enums.CoverTypes;
import com.somegeorge.labs.data.enums.Lab23EditMode;
import com.somegeorge.labs.data.models.Author;
import com.somegeorge.labs.data.models.Book;
import com.somegeorge.labs.data.models.Publisher;
import com.somegeorge.labs.data.repositories.IAuthorRepository;
import com.somegeorge.labs.data.repositories.IBookRepository;
import com.somegeorge.labs.data.repositories.IPublisherRepository;
import com.somegeorge.labs.utils.di.DaggerIDaggerComponent;
import com.somegeorge.labs.utils.di.modules.RepositoryModule;

import java.security.PublicKey;
import java.util.Arrays;
import java.util.List;

import javax.inject.Inject;

/**
 * Created by georg on 22.10.2017.
 */

public class Lab23EditViewModel extends BaseObservable {

    @Inject public IBookRepository bookRepository;
    @Inject public IAuthorRepository authorRepository;
    @Inject public IPublisherRepository publisherRepository;

    private List<CoverTypes> covers = Arrays.asList(CoverTypes.Soft, CoverTypes.Hard);

    private Lab23EditMode mode;
    private Activity context;
    private Book book;

    private int selectedAuthorPosition;
    private int selectedPublisherPosition;
    private int selectedCoverPosition;

    private String stringPrice;
    private String stringYear;
    private String stringPages;

    public boolean isCreateMode() {
        return mode == Lab23EditMode.CREATE;
    }

    public Book getBook() {
        return book;
    }

    public void setBook(Book book) {
        this.book = book;
    }

    @Bindable
    public String getPrice(){
        return this.stringPrice;
    }
    public void setPrice(String price){
        this.stringPrice = price;
    }

    @Bindable
    public String getYear(){
        return this.stringYear;
    }
    public void setYear(String year){
        this.stringYear = year;
    }

    @Bindable
    public String getPage(){
        return this.stringPages;
    }
    public void setPage(String pages){
        this.stringPages = pages;
    }



    public List<Author> getAuthors(){
        return authorRepository.getAll();
    }

    public List<Publisher> getPublishers(){
        return publisherRepository.getAll();
    }

    public List<CoverTypes> getCovers() {
        return covers;
    }

    public Lab23EditViewModel(Book book, Activity parent) {
        ((Startup)parent.getApplication()).getComponent().inject(this);
        this.context = parent;

        if(book == null) {
            this.mode = Lab23EditMode.CREATE;
            this.book = new Book();
        }
        else {
            this.mode = Lab23EditMode.EDIT;
            this.book = book;
            this.stringPrice = String.valueOf(book.getPrice());
            this.stringYear = String.valueOf(book.getYear());
            this.stringPages = String.valueOf(book.getPagesCount());
            this.setSelectedAuthorPosition(book.getAuthorId());
            this.setSelectedPublisherPosition(book.getPublisherId());
        }
    }

    public void Finish(){

        if(this.stringPages == null
                || (this.stringPrice == null)
                || (this.stringYear == null)
                || (this.book.getName() == null))
        {
            //TODO: Handle Validation
            Toast.makeText(context, context.getString(R.string.validation_error), Toast.LENGTH_SHORT).show();
            return;
        }

        this.book.setPrice(Double.parseDouble(this.stringPrice));
        this.book.setYear(Integer.parseInt(this.stringYear));
        this.book.setPagesCount(Integer.parseInt(this.stringPages));


        if(this.mode == Lab23EditMode.CREATE) {
            bookRepository.add(book);
        }
        else {
            bookRepository.update(book.getId(), book);
        }
        context.finish();
    }



    @Bindable
    public int getSelectedAuthorPosition() {
        return selectedAuthorPosition;
    }

    public void setSelectedAuthorPosition(int selectedAuthorPosition) {
        this.selectedAuthorPosition = selectedAuthorPosition;
        book.setAuthor(authorRepository.get(selectedAuthorPosition));
        book.setAuthorId(book.getAuthor().getId());
    }
    @Bindable
    public int getSelectedPublisherPosition() {
        return selectedPublisherPosition;
    }

    public void setSelectedPublisherPosition(int selectedPublisherPosition) {
        this.selectedPublisherPosition = selectedPublisherPosition;
        book.setPublisher(publisherRepository.get(selectedPublisherPosition));
        book.setPublisherId(book.getPublisher().getId());
    }
    @Bindable
    public int getSelectedCoverPosition() {
        return selectedCoverPosition;
    }

    public void setSelectedCoverPosition(int selectedCoverPosition) {
        this.selectedCoverPosition = selectedCoverPosition;
        book.setCover(covers.get(selectedCoverPosition));
    }

    @BindingAdapter("selectedItemPositionAttrChanged")
    public void setSelectedItemPositionListener(AppCompatSpinner view, final InverseBindingListener selectedItemPositionChange) {
        if (selectedItemPositionChange == null) {
            view.setOnItemSelectedListener(null);
        } else {
            view.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    selectedItemPositionChange.onChange();

                    if(view.getId() == R.id.authorList)
                        setSelectedAuthorPosition(position);
                    if(view.getId() == R.id.publisherList)
                        setSelectedPublisherPosition(position);
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });
        }
    }

    @InverseBindingAdapter(attribute = "selectedItemPosition")
    public static int getSelectedItemPosition(AppCompatSpinner spinner) {
        return spinner.getSelectedItemPosition();
    }
}
