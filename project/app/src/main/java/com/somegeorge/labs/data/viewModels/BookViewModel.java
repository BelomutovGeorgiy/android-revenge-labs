package com.somegeorge.labs.data.viewModels;

import android.app.Activity;
import android.content.Intent;
import android.databinding.BaseObservable;
import android.os.Bundle;

import com.somegeorge.labs.Lab23EditActivity;
import com.somegeorge.labs.adapters.RecyclerClickListener;
import com.somegeorge.labs.data.models.Book;

/**
 * Created by georg on 24.10.2017.
 */

public class BookViewModel extends BaseBookViewModel {
    private Book book;
    private RecyclerClickListener bookClickListener;

    public BookViewModel(Activity parent, Book book) {
        super(parent);
        this.book = book;
    }

    public Book getBook() {
        return book;
    }

    public void setBook(Book book) {
        this.book = book;
    }

    public void bookUpdate(){
        Intent intent = new Intent(context, Lab23EditActivity.class);
        Bundle bundle = new Bundle();
        bundle.putSerializable("BOOK", book);
        intent.putExtras(bundle);
        context.startActivity(intent);
    }
}
