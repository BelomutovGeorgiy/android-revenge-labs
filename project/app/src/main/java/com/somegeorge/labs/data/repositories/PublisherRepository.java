package com.somegeorge.labs.data.repositories;

import com.somegeorge.labs.data.models.Publisher;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collector;
import java.util.stream.Collectors;

import javax.inject.Inject;

import dagger.Module;
import dagger.Provides;

/**
 * Created by georg on 22.10.2017.
 */
public class PublisherRepository implements IPublisherRepository {

    private List<Publisher> publishers;

    @Inject
    public PublisherRepository() {
        publishers = Arrays.asList(
                new Publisher(0, "Azbuka"),
                new Publisher(1, "Kamilfo"),
                new Publisher(2, "Izdatelstvo"),
                new Publisher(3, "Zapiski Zhmyha")
        );
    }

    @Override
    public List<Publisher> getAll() {
        return this.publishers;
    }

    @Override
    public Publisher get(int id) {
        return publishers.stream().filter(p -> p.getId() == id).collect(Collectors.toList()).get(0);
    }

    @Override
    public void update(int id, Publisher object) {

    }

    @Override
    public void add(Publisher object) {
        this.publishers.add(object);
    }

    @Override
    public void delete(int id) {

    }
}
