package com.somegeorge.labs.activities.lab2Activities;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import com.somegeorge.labs.R;
import com.somegeorge.labs.data.models.Book;
import com.somegeorge.labs.data.viewModels.lab22ViewModels.Lab221ViewModel;
import com.somegeorge.labs.databinding.ActivityLab221Binding;

import java.util.Arrays;
import java.util.List;

/**
 * Created by georg on 21.10.2017.
 */

public class Lab221Activity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ActivityLab221Binding binding = DataBindingUtil.setContentView(this, R.layout.activity_lab_2_2_1);
        //List<Book> books = (List<Book>)this.getIntent().getExtras().getSerializable("LIST");
        List<Book> books = Arrays.asList(
                new Book(6, "Kenobi", 1, 0, 2010),
                new Book(7, "Scoundrels", 2, 0, 2010),
                new Book(8, "Aftermath", 3, 0, 2015),
                new Book(9, "Tarkin", 4, 0, 2014),
                new Book(9, "Darth Plagues", 4, 0, 2013),
                new Book(9, "Aftermath: Life depth", 3, 0, 2016));
        Log.d("LAB221", "what is books? it is "+books);
        Lab221ViewModel model = new Lab221ViewModel(books, this);
        binding.setBinding(model);

    }

}
