package com.somegeorge.labs.data.repositories;

import com.somegeorge.labs.data.models.Author;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import javax.inject.Inject;

import dagger.Module;
import dagger.Provides;

/**
 * Created by georg on 22.10.2017.
 */

public class AuthorRepository implements IAuthorRepository {

    @Inject
    public AuthorRepository() {
        authors = Arrays.asList(
                new Author(0, "Drew","Karpyshn"),
                new Author(1, "John","Jackson Miller"),
                new Author(2, "Timoty","Zahn"),
                new Author(3, "Chuck","Wending"),
                new Author(4, "James", "Luceno")
        );
    }

    private List<Author> authors;

    @Override
    public List<Author> getAll() {
        return this.authors;
    }

    @Override
    public Author get(int id) {
        return this.authors.stream().filter(a -> a.getId() == id).collect(Collectors.toList()).get(0);
    }

    @Override
    public void update(int id, Author object) {

    }

    @Override
    public void add(Author object) {
        this.authors.add(object);
    }

    @Override
    public void delete(int id) {

    }
}
