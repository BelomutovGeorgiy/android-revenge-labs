package com.somegeorge.labs.data.repositories;

import android.util.Log;

import com.somegeorge.labs.data.models.Book;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import javax.inject.Inject;
import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Created by georg on 22.10.2017.
 */

@Singleton
public class BookRepository implements IBookRepository {

    private ArrayList<Book> books;

    @Inject
    public BookRepository() {
        books = new ArrayList<Book>(){{
                        add( new Book(0, "Lost Tribe of The Sith", 1, 0, 2006));
                        add(new Book(1, "Darth Bane: Path of Destruction", 0, 2, 2007));
                        add(new Book(2, "Darth Bane: Rule of Two", 0, 1, 2008));
                        add(new Book(4, "Darth Bane: Dynasty of Evil", 0, 0, 2009));
                        add(new Book(5, "Allegiance", 2, 0, 2006));
                        add(new Book(6, "Kenobi", 1, 0, 2010));
                        add(new Book(7, "Scoundrels", 2, 1, 2010));
                        add(new Book(8, "Aftermath", 3, 0, 2015));
                        add(new Book(9, "Tarkin", 4, 0, 2014));
                        add(new Book(9, "Darth Plagues", 4, 2, 2013));
                        add(new Book(9, "Aftermath: Life depth", 3, 0, 2016));
                        add(new Book(9, "Rogue-One: Catalist", 4, 3, 2016));
            }};
        books.forEach(b -> b.setPrice(9.99));
    }

    @Override
    public List<Book> getAllByAuthorId(int authorId) {
        return this.books.stream().filter(b -> b.getAuthorId() == authorId).collect(Collectors.toList());
    }

    @Override
    public List<Book> getAllAfterSelectedYear(int year) {
        return this.books.stream().filter(b -> b.getYear() > year).collect(Collectors.toList());
    }

    @Override
    public List<Book> getAllByPublisherId(int publisherId) {
        return this.books.stream().filter(b -> b.getPublisherId() == publisherId).collect(Collectors.toList());
    }

    @Override
    public List<Book> getAll() {
        return this.books;
    }

    @Override
    public Book get(int id) {
        return this.books.stream().filter(b -> b.getId() == id).collect(Collectors.toList()).get(0);
    }

    @Override
    public void update(int id, Book object) {
        int position = books.indexOf(books.stream().filter(b -> b.getId() == id).collect(Collectors.toList()).get(0));
        books.set(position, object);
    }

    @Override
    public void add(Book object) {
        object.setId( books.get(books.size()-1).getId() + 1);
        books.add(object);
    }

    @Override
    public void delete(int id) {

    }
}
