package com.somegeorge.labs.data.viewModels.lab23ViewModels;

import android.app.Activity;
import android.content.Intent;
import android.databinding.InverseBindingMethod;
import android.databinding.InverseBindingMethods;
import android.os.Bundle;
import android.support.v7.widget.AppCompatSpinner;

import com.somegeorge.labs.Lab23EditActivity;
import com.somegeorge.labs.data.models.Book;
import com.somegeorge.labs.data.viewModels.BaseBookViewModel;
import com.somegeorge.labs.utils.di.DaggerIDaggerComponent;
import com.somegeorge.labs.utils.di.modules.RepositoryModule;

import java.util.List;

/**
 * Created by georg on 22.10.2017.
 */
@InverseBindingMethods({
        @InverseBindingMethod(type = AppCompatSpinner.class, attribute = "android:selectedItemPosition")
})
public class Lab23ViewModel extends BaseBookViewModel {

    public Lab23ViewModel(Activity parent) {
        super(parent);
    }

    public List<Book> getBooks(){
        return bookRepository.getAll();
    }

    public void createBook(){
        Intent intent = new Intent(context, Lab23EditActivity.class);
        Bundle bundle = new Bundle();
        bundle.putSerializable("BOOK", null);
        intent.putExtras(bundle);
        context.startActivity(intent);
    }

}
