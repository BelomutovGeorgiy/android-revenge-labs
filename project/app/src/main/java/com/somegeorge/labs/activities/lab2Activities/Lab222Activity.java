package com.somegeorge.labs.activities.lab2Activities;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import com.somegeorge.labs.R;
import com.somegeorge.labs.data.models.Book;
import com.somegeorge.labs.data.viewModels.lab22ViewModels.Lab221ViewModel;
import com.somegeorge.labs.databinding.ActivityLab221Binding;
import com.somegeorge.labs.databinding.ActivityLab222Binding;

import java.util.Arrays;
import java.util.List;

/**
 * Created by georg on 21.10.2017.
 */

public class Lab222Activity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ActivityLab222Binding binding = DataBindingUtil.setContentView(this, R.layout.activity_lab_2_2_2);
        // List<Book> books = (List<Book>)this.getIntent().getExtras().getSerializable("LIST");
        List<Book> books = Arrays.asList(
                new Book(0, "Lost Tribe of The Sith", 1, 0, 2006),
                new Book(1, "Darth Bane: Path of Destruction", 0, 0, 2007),
                new Book(2, "Darth Bane: Rule of Two", 0, 0, 2008),
                new Book(4, "Darth Bane: Dynasty of Evil", 0, 0, 2009),
                new Book(5, "Allegiance", 2, 0, 2006)
        );
        Lab221ViewModel model = new Lab221ViewModel(books, this);
        binding.setBinding(model);

    }

}
