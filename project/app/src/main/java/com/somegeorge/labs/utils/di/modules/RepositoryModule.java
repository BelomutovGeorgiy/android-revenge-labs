package com.somegeorge.labs.utils.di.modules;

import android.util.Log;

import com.android.annotations.NonNull;
import com.somegeorge.labs.data.repositories.AuthorRepository;
import com.somegeorge.labs.data.repositories.BookRepository;
import com.somegeorge.labs.data.repositories.IAuthorRepository;
import com.somegeorge.labs.data.repositories.IBookRepository;
import com.somegeorge.labs.data.repositories.IPublisherRepository;
import com.somegeorge.labs.data.repositories.PublisherRepository;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Created by georg on 22.10.2017.
 */

@Module
public class RepositoryModule {

    private BookRepository bookRepository;
    private AuthorRepository authorRepository;
    private PublisherRepository publisherRepository;

    public RepositoryModule(){
        Log.d("REPOSITORY MODULE", "I'm Created");
        bookRepository = new BookRepository();
        authorRepository = new AuthorRepository();
        publisherRepository = new PublisherRepository();

    }

    @Provides
    @NonNull
    @Singleton
    IBookRepository provideBookRepository(){
        return bookRepository;
    }

    @Provides
    @NonNull
    @Singleton
    IAuthorRepository authorRepository(){
        return authorRepository;
    }

    @Provides
    @NonNull
    @Singleton
    IPublisherRepository publisherRepository(){
        return publisherRepository;
    }
}
