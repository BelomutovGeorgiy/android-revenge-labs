package com.somegeorge.labs;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import com.somegeorge.labs.data.models.Book;
import com.somegeorge.labs.data.viewModels.lab22ViewModels.Lab221ViewModel;
import com.somegeorge.labs.data.viewModels.lab23ViewModels.Lab23EditViewModel;
import com.somegeorge.labs.databinding.ActivityLab222Binding;
import com.somegeorge.labs.databinding.ActivityLab23CreateBinding;

import java.util.Arrays;
import java.util.List;

/**
 * Created by georg on 22.10.2017.
 */

public class Lab23EditActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ActivityLab23CreateBinding binding = DataBindingUtil.setContentView(this, R.layout.activity_lab2_3_create);
        Book book = (Book) this.getIntent().getExtras().getSerializable("BOOK");
        Lab23EditViewModel model = new Lab23EditViewModel(book, this);
        binding.setBinding(model);
    }
}
