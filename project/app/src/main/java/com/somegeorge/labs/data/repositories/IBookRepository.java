package com.somegeorge.labs.data.repositories;

import com.somegeorge.labs.data.models.Book;

import java.util.Date;
import java.util.List;

/**
 * Created by georg on 22.10.2017.
 */

public interface IBookRepository extends IRepository<Book> {
    List<Book> getAllByAuthorId(int authorId);
    List<Book> getAllAfterSelectedYear(int year);
    List<Book> getAllByPublisherId(int publisherId);
}
