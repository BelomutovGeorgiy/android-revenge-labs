package com.somegeorge.labs.data.models;

import java.io.Serializable;
import java.util.List;

/**
 * Created by georg on 15.10.2017.
 */

public class Author implements Serializable {

    private String name;
    private String surname;
    private List<Book> books;

    private int id;

    public Author(int id, String name, String surname) {
        this.name = name;
        this.surname = surname;
        this.id = id;
        this.books = books;
    }

    public int getId() { return id; }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public List<Book> getBooks() {
        return books;
    }

    public void setBooks(List<Book> books) {
        this.books = books;
    }



    public String getFullName(){
        return String.format("%s %s", this.name, this.surname);
    }

    @Override
    public String toString() {
        return String.format("%s %s",name, surname);
    }
}
