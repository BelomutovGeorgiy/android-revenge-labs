package com.somegeorge.labs.data.viewModels;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

import com.somegeorge.labs.R;
import com.somegeorge.labs.activities.lab2Activities.Lab221Activity;

import java.io.Serializable;

/**
 * Created by georg on 21.10.2017.
 */

public class Lab22ViewModel extends Lab2ViewModel {

    public Lab22ViewModel(Activity parent) {
        super(parent);
    }



    public void showBooks(){
        setSelectedAuthor(authors.get(selectedAuthorPosition));
        Intent bookList = new Intent(Intent.ACTION_VIEW);
        Bundle bundle = new Bundle();
        bundle.putSerializable("LIST", (Serializable)books);
        bookList.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.getApplicationContext().startActivity(bookList);
    }



}
