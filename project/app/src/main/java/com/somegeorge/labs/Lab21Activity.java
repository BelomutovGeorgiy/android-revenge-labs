package com.somegeorge.labs;

import android.app.Fragment;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.somegeorge.labs.data.viewModels.Lab21ViewModel;
import com.somegeorge.labs.data.viewModels.Lab2ViewModel;
import com.somegeorge.labs.databinding.ActivityLab2Binding;

/**
 * Created by georg on 15.10.2017.
 */

public class Lab21Activity extends Fragment {

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        return inflater.inflate(R.layout.activity_lab2, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState){
        super.onViewCreated(view, savedInstanceState);
        ActivityLab2Binding binding = DataBindingUtil.bind(view);
        binding.setBinding(new Lab21ViewModel(this.getActivity()));
    }
}
