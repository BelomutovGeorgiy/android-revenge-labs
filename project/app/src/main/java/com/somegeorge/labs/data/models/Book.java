package com.somegeorge.labs.data.models;

import com.somegeorge.labs.data.enums.CoverTypes;

import java.io.Serializable;

/**
 * Created by georg on 15.10.2017.
 */
public class Book implements Serializable {

    private int id;
    private String name;
    private int authorId;
    private int publisherId;
    private int year;
    private int pagesCount;
    private CoverTypes cover;

    private double price;

    private Author author;
    private Publisher publisher;

    public Book(int id, String name, int authorId, int publisherId, int year) {
        this.id = id;
        this.name = name;
        this.authorId = authorId;
        this.publisherId = publisherId;
        this.year = year;
    }

    public Book(){}

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAuthorId() {
        return authorId;
    }

    public void setAuthorId(int author) {
        this.authorId = author;
    }

    public int getPublisherId() {
        return publisherId;
    }

    public void setPublisherId(int publisherId) {
        this.publisherId = publisherId;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public Author getAuthor() {
        return author;
    }

    public void setAuthor(Author author) {
        this.author = author;
    }

    public Publisher getPublisher() {
        return publisher;
    }

    public void setPublisher(Publisher publisher) {
        this.publisher = publisher;
    }

    public int getPagesCount() {
        return pagesCount;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public void setPagesCount(int pagesCount) {
        this.pagesCount = pagesCount;
    }

    public CoverTypes getCover() {
        return cover;
    }

    public void setCover(CoverTypes cover) {
        this.cover = cover;
    }
}
